const Student = require("../models/Student.js")

exports.get_students = async function(req,res){
    const studentList = await Student.find()
    res.send(studentList)
    res.end()
}

exports.add_student = async function(req,res){
    const student = new Student({
        fullName: req.body.fullName,
        address: req.body.address,
        gender: req.body.gender,
        age: req.body.age
    })
    await student.save()
    res.sendStatus(201)
    res.end()
}

exports.edit_student = async function(req,res){
    await Student.updateOne({
        _id: req.body._id
    },{
        fullName: req.body.fullName,
        address: req.body.address,
        gender: req.body.gender,
        age: req.body.age
    })
    res.end()
}

exports.delete_student = async function(req,res){
    await Student.deleteOne({
        _id: req.body._id
    })
    res.end()
}