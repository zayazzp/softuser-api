const User = require("../models/User.js")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")

exports.register_user = async function(req,res){
    const hashed = await bcrypt.hash(req.body.password, 10)
    const user = new User({
        fname: req.body.fname,
        lname: req.body.lname,
        username: req.body.username,
        password: hashed
    })
    await user.save()
    res.end()
}

exports.login_user = async function(req,res){
    const user = await User.findOne({username: req.body.username})
    if(user){
        const validLogin = await bcrypt.compare(req.body.password, user.password)
        if(validLogin){
            const accessToken = jwt.sign({_id: user._id}, "secretsecret")
            res.json({accessToken})
        }
        else{
            res.status(406)
        }
    }
    else{
        res.status(404)
    }
    res.end()
}