const express = require("express")
require("dotenv").config()
const connection = require("./utils/dbConnection.js")

// Routes Import
const studentRoutes = require("./routes/studentRoute.js")
const userRoutes = require("./routes/userRoute.js")

const app = express()
const port = 5500
app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.use("/student", studentRoutes)
app.use("/user", userRoutes)

connection().then(()=>{
    app.listen(port, ()=>{
        console.log(`Listening at port http://localhost:${port}`)
    })
})