const router = require("express").Router()
const studentController = require("../controllers/studentController.js")
router.get("/", studentController.get_students)

router.post("/", studentController.add_student)

router.put("/", studentController.edit_student)

router.delete("/", studentController.delete_student)

module.exports = router