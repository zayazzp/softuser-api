const mongoose = require("mongoose")

const studentSchema = new mongoose.Schema({
    fullName:String,
    address: String,
    gender: {
        type: String,
        enum: ["male", "female", "other"]
    },
    age: Number
})

module.exports = mongoose.model("Student", studentSchema)