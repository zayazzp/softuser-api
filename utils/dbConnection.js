const mongoose = require("mongoose")
const URI = process.env.DB_URI
module.exports = () =>{
    console.log("Connecting to database")
    return mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    })
}